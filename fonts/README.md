# Instalação

Instale estas fontes antes de editar os arquivos SVG ou XCF. Em GNU/Linux, copie os arquivos para o diretório `fonts` na sua home.

# Licenças das fontes

* Facile Sans: *free for personal and commercial use*
  (https://fontdation.com/facile-sans/)
