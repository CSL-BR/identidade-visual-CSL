# Identidade visual da CSL

Contém arquivos SVG, PNG, XCF e fontes para identificar todo o material
produzido pelas CSLs. 

Licença: CC0 (domínio público).
